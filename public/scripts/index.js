// convert epochtime to JavaScripte Date object
function epochToJsDate(epochTime){
    return new Date(epochTime*1000);
  }
  
  // convert time to human-readable format YYYY/MM/DD HH:MM:SS
  function epochToDateTime(epochTime){
    var epochDate = new Date(epochToJsDate(epochTime));
    var dateTime = epochDate.getFullYear() + "/" +
      ("00" + (epochDate.getMonth() + 1)).slice(-2) + "/" +
      ("00" + epochDate.getDate()).slice(-2) + " " +
      ("00" + epochDate.getHours()).slice(-2) + ":" +
      ("00" + epochDate.getMinutes()).slice(-2) + ":" +
      ("00" + epochDate.getSeconds()).slice(-2);
  
    return dateTime;
  }
  
  // function to plot values on charts
  function plotValues(chart, timestamp, value){
    var x = epochToJsDate(timestamp).getTime();
    var y = Number (value);
    if(chart.series[0].data.length > 40) {
      chart.series[0].addPoint([x, y], true, true, true);
    } else {
      chart.series[0].addPoint([x, y], true, false, true);
    }
  }
//Buttons 
// Elements for GPIO states
const stateElement1 = document.getElementById("state1");
const stateElement2 = document.getElementById("state2");
const stateElement3 = document.getElementById("state3");
//Elements for level switch states (the pond, rainbarrell and drain level switch)
const stateElement4 = document.getElementById("state4");
const stateElement5 = document.getElementById("state5");
const stateElement6 = document.getElementById("state6");
const stateElementLevel=document.getElementById("level");

// Button Elements
const btn1On = document.getElementById('btn1On');
const btn1Off = document.getElementById('btn1Off');
const btn2On = document.getElementById('btn2On');
const btn2Off = document.getElementById('btn2Off');
const btn3On = document.getElementById('btn3On');
const btn3Off = document.getElementById('btn3Off');


//End Buttons

  // DOM elements
  const loginElement = document.querySelector('#login-form');
  const contentElement = document.querySelector("#content-sign-in");
  const userDetailsElement = document.querySelector('#user-details');
  const authBarElement = document.querySelector('#authentication-bar');
  const deleteButtonElement = document.getElementById('delete-button');
  const deleteModalElement = document.getElementById('delete-modal');
  const deleteDataFormElement = document.querySelector('#delete-data-form');
  const viewDataButtonElement = document.getElementById('view-data-button');
  const hideDataButtonElement = document.getElementById('hide-data-button');
  const tableContainerElement = document.querySelector('#table-container');
  const chartsRangeInputElement = document.getElementById('charts-range');
  const loadDataButtonElement = document.getElementById('load-data');
  const buttonsCheckboxElement = document.querySelector('input[name=buttons-checkbox]');
  const cardsCheckboxElement = document.querySelector('input[name=cards-checkbox]');
  const gaugesCheckboxElement = document.querySelector('input[name=gauges-checkbox]');
  const chartsCheckboxElement = document.querySelector('input[name=charts-checkbox]');
  const imagesCheckboxElement = document.querySelector('input[name=images-checkbox]');
  
  // DOM elements for sensor readings
  const buttonsReadingsElement = document.querySelector("#buttons-div");
  const cardsReadingsElement = document.querySelector("#cards-div");
  const cardsReadingsElement2= document.querySelector("#cards2-div");
  const imagesReadingsElement = document.querySelector('#images-div');
  const gaugesReadingsElement = document.querySelector("#gauges-div");
  const chartsDivElement = document.querySelector('#charts-div');
  const tempElement = document.getElementById("temp");
  const fahrenheitElement=document.getElementById("fahrenheit");
  const humElement = document.getElementById("hum");
  const dewElement =document.getElementById("dew");// dew is de HTML id for the Dewpoint
  const moistElement = document.getElementById("moisture")
  const presElement = document.getElementById("pres");
  const mmHgElement =document.getElementById("mmHg");
  const updateElement = document.getElementById("lastUpdate")
  
  
  // MANAGE LOGIN/LOGOUT UI
  const setupUI = (user) => {
    if (user) {
      //toggle UI elements
      loginElement.style.display = 'none';//means not visible
      contentElement.style.display = 'block';
      authBarElement.style.display ='block';
      userDetailsElement.style.display ='block';
      userDetailsElement.innerHTML = user.email;
  
      // get user UID to get data from database
      var uid = user.uid;
      console.log(uid);
  
      // Database paths (with user UID)
      var dbPath = 'UsersData/' + uid.toString() + '/readings';
      var chartPath = 'UsersData/' + uid.toString() + '/charts/range';
  
      // Database references
      var dbRef = firebase.database().ref(dbPath);
      var chartRef = firebase.database().ref(chartPath);

      //Buttons
      // Database path for GPIO states
  var dbPathOutput1 = 'UsersData/' + uid.toString() +'/outputs/digital/12';//button
  var dbPathOutput2 = 'UsersData/' + uid.toString() +'/outputs/digital/13';//button
  var dbPathOutput3 = 'UsersData/' + uid.toString() +'/outputs/digital/14';//button
  var dbPathOutput4 = 'UsersData/' + uid.toString() +'/inputs/digital/drainpipe';//levelswitch
  var dbPathOutput5 = 'UsersData/' + uid.toString() +'/inputs/digital/rainbarrel';//levelswitch
  var dbPathOutput6 = 'UsersData/' + uid.toString() +'/inputs/digital/pond';//levelswitch
  var dbPathInputLevel = 'board2/outputs/digital/5';// not used

var dbRefOutput1 = firebase.database().ref().child(dbPathOutput1);//12
var dbRefOutput2 = firebase.database().ref().child(dbPathOutput2);//13
var dbRefOutput3 = firebase.database().ref().child(dbPathOutput3);//14
var dbRefOutput4 = firebase.database().ref().child(dbPathOutput4);//drainpipe
var dbRefOutput5 = firebase.database().ref().child(dbPathOutput5);//rainbarrel
var dbRefOutput6 = firebase.database().ref().child(dbPathOutput6);//pond
var dbRefInputLevel = firebase.database().ref().child(dbPathInputLevel);//not used


//Update states depending on the database value, change into appropriate picture
dbRefOutput1.on('value', snap => {
  if(snap.val()==1) {
      stateElement1.innerText="ON";
      var watershown = '/images/vijverpomp-on.png'
  }
  else{
      stateElement1.innerText="OFF";
      var watershown = "/images/vijverpomp-off.png"
      
  }
  document.getElementById('img-w').src = watershown;// injects the proper picture in the html code
});

dbRefOutput2.on('value', snap => {
  if(snap.val()==1) {
      stateElement2.innerText="ON";
      var airshown = '/images/airpump-on.png'
  }
  else{
      stateElement2.innerText="OFF";
      var airshown = "/images/airpump-off.png"
  }
  document.getElementById('img-a').src = airshown;
});
dbRefOutput3.on('value', snap => {
  if(snap.val()==1) { 
      stateElement3.innerText="ON";
      var lightshown='images/lightbulb-on.jpg'
  }
  else{
      stateElement3.innerText="OFF";
      var lightshown='images/lightbulb-off.jpg'
  }
  document.getElementById('img-l').src = lightshown;
});

dbRefOutput4.on('value', snap => {
  if(snap.val()==1) {            //it is a boolean field, so could just test for true/false
      stateElement4.innerText="Open";
      var levelshown = "/images/drainpipe-free.jpg"
  }
  else{

      stateElement4.innerText="Clogged";

      var levelshown = "/images/drainpipe-blocked.jpg"
  }
  document.getElementById('img-d').src = levelshown;
});

dbRefOutput5.on('value', snap => {
  if(snap.val()) {              //it's a boolean, so can just test for true or not
      stateElement5.innerText="Not Full";
     var barrelshown = "/images/tank-empty.png"
     console.log("tank/empty");
  }
  else{
      stateElement5.innerText="Full";
      console.log("tank/full");
     var barrelshown = "/images/tank-full.png"
  }
  document.getElementById('img-r').src = barrelshown;
});

dbRefOutput6.on('value',snap=>{
if (snap.val()) {
  stateElement6.innerHTML="full";
  var imageshown = "/images/full.png"
  console.log("waar/full");
} else {
   stateElement6.innerHTML="<span style='color:red'><i>not full</i></span>";
   var imageshown = "/images/empty.png"
   console.log("niet waar/empty");
}
    document.getElementById('img').src = imageshown;
    });

    //Not used
dbRefInputLevel.on('value', snap => {
  
});


 // Update database uppon button click
 btn1On.onclick = () =>{
  dbRefOutput1.set(1);
}
btn1Off.onclick = () =>{
  dbRefOutput1.set(0);
}

btn2On.onclick = () =>{
  dbRefOutput2.set(1);
}
btn2Off.onclick = () =>{
  dbRefOutput2.set(0);
}

btn3On.onclick = () =>{
  dbRefOutput3.set(1);
}
btn3Off.onclick = () =>{
  dbRefOutput3.set(0);
}
//---------------
  
      // CHARTS
      // Number of readings to plot on charts
      var chartRange = 0;
      // Get number of readings to plot saved on database (runs when the page first loads and whenever there's a change in the database)
      chartRef.on('value', snapshot =>{
        chartRange = Number(snapshot.val());
        console.log(chartRange);
        // Delete all data from charts to update with new values when a new range is selected
        chartT.destroy();
        chartH.destroy();
        chartP.destroy();
        // Render new charts to display new range of data
        chartT = createTemperatureChart();
        chartH = createHumidityChart();
        chartP = createPressureChart();
        // Update the charts with the new range
        // Get the latest readings and plot them on charts (the number of plotted readings corresponds to the chartRange value)
        dbRef.orderByKey().limitToLast(chartRange).on('child_added', snapshot =>{
          var jsonData = snapshot.toJSON(); // example: {temperature: 25.02, humidity: 50.20, pressure: 1008.48, timestamp:1641317355}
          // Save values on variables
          var temperature = jsonData.temperature;
          var humidity = jsonData.humidity;
          var pressure = jsonData.pressure;
          var timestamp = jsonData.timestamp;
          // Plot the values on the charts
          plotValues(chartT, timestamp, temperature);
          plotValues(chartH, timestamp, humidity);
          plotValues(chartP, timestamp, pressure);
        });
      });
  
      // Update database with new range (input field)
      chartsRangeInputElement.onchange = () =>{
        chartRef.set(chartsRangeInputElement.value);
      };
  
      //CHECKBOXES
      buttonsCheckboxElement.addEventListener('change', (e) =>{
        if (buttonsCheckboxElement.checked) {
          buttonsReadingsElement.style.display = 'block';
          //cardsReadingsElement2.style.display = 'block';
        }
        else{
          buttonsReadingsElement.style.display = 'none';
          //cardsReadingsElement2.style.display= 'none';
        }
      });
      // Checbox (cards for sensor readings)
      cardsCheckboxElement.addEventListener('change', (e) =>{
        if (cardsCheckboxElement.checked) {
          cardsReadingsElement.style.display = 'block';
          cardsReadingsElement2.style.display = 'block';
        }
        else{
          cardsReadingsElement.style.display = 'none';
          cardsReadingsElement2.style.display= 'none';
        }
      });
      // Checkbox images
      imagesCheckboxElement.addEventListener('change',(e) =>{
      if (imagesCheckboxElement.checked) {
        imagesReadingsElement.style.display='block';
      }
      else{
         imagesReadingsElement.style.display='none';
      }
      });

      // Checbox (gauges for sensor readings)
      gaugesCheckboxElement.addEventListener('change', (e) =>{
        if (gaugesCheckboxElement.checked) {
          gaugesReadingsElement.style.display = 'block';
        }
        else{
          gaugesReadingsElement.style.display = 'none';
        }
      });
      // Checbox (charta for sensor readings)
      chartsCheckboxElement.addEventListener('change', (e) =>{
        if (chartsCheckboxElement.checked) {
          chartsDivElement.style.display = 'block';
        }
        else{
          chartsDivElement.style.display = 'none';
        }
      });
  
      // CARDS
      // Get the latest readings and display on cards
      dbRef.orderByKey().limitToLast(1).on('child_added', snapshot =>{
        var jsonData = snapshot.toJSON(); // example: {temperature: 25.02, humidity: 50.20, pressure: 1008.48, timestamp:1641317355}
        var temperature = parseFloat(jsonData.temperature).toFixed(1);
        var fahrenheit=parseFloat(jsonData.temperature*1.8+32).toFixed(1);
        var humidity = parseFloat(jsonData.humidity).toFixed(0);
        var moisture =jsonData.moisture;
        var dew=parseFloat(((17.625*parseFloat(jsonData.temperature)/(parseFloat(jsonData.temperature)+243.04))+Math.log(parseFloat(jsonData.humidity)/100))*(243.04/(17.625-(17.625*parseFloat(jsonData.temperature)/(parseFloat(jsonData.temperature)+243.04))+Math.log(parseFloat(jsonData.humidity)/100)))).toFixed(2);
        //((17.625*jsonData.temperature)/(jsonData.temperature+243.04))+Math.log(humidity/100))*(243.04/(17.625-(17.625*temperature/(temperature+243.04))+Math.log(humidity/100)));
        var pressure = parseFloat(jsonData.pressure).toFixed(0);
        var mmHg= parseFloat(pressure/1.333224).toFixed(0);
        c=jsonData.level;// used for the level of the pond
        var timestamp = jsonData.timestamp;
        // Update DOM elements
        tempElement.innerHTML = temperature;
        fahrenheitElement.innerHTML = fahrenheit; //documnt.getElementById("fahrenheit").innerHTML=fahrenheit;
        humElement.innerHTML = humidity;
        dewElement.innerHTML= dew;// is infact document.getElementById("dew").innerHTML=dew; met de eerste 'dew' de HTML id en de 2e de variabele
        moistElement.innerHTML=moisture;
        
        presElement.innerHTML = pressure;
        mmHgElement.innerHTML = mmHg;
        updateElement.innerHTML = epochToDateTime(timestamp);
        
      });
  
      // GAUGES
      // Get the latest readings and display on gauges
      dbRef.orderByKey().limitToLast(1).on('child_added', snapshot =>{
        var jsonData = snapshot.toJSON(); // example: {temperature: 25.02, humidity: 50.20, pressure: 1008.48, timestamp:1641317355}
        var temperature = jsonData.temperature;
        var humidity = jsonData.humidity;
        var pressure = jsonData.pressure;
        var mmHg= parseFloat(pressure/1.333224).toFixed(0);
        var dew=parseFloat(((17.625*parseFloat(jsonData.temperature)/(parseFloat(jsonData.temperature)+243.04))+Math.log(parseFloat(jsonData.humidity)/100))*(243.04/(17.625-(17.625*parseFloat(jsonData.temperature)/(parseFloat(jsonData.temperature)+243.04))+Math.log(parseFloat(jsonData.humidity)/100)))).toFixed(2);
        var timestamp = jsonData.timestamp;
        // Update DOM elements
        var gaugeT = createTemperatureGauge();
        var gaugeH = createHumidityGauge();
        var gaugeP = createPressureGauge();
        var gaugeK = createmmHgGauge();
        var gaugeD = createDewGauge();
        //var gaugeO = createOtherChart();
        gaugeT.draw();
        gaugeH.draw();
        gaugeP.draw();
        gaugeK.draw();
        gaugeD.draw();
        //gaugeO.draw();
        gaugeT.value = temperature;
        gaugeH.value = humidity;
        gaugeP.value = pressure;
        gaugeK.value = mmHg;
        gaugeD.value = dew;
        updateElement.innerHTML = epochToDateTime(timestamp);
      });
  
      // DELETE DATA
      // Add event listener to open modal when click on "Delete Data" button
      deleteButtonElement.addEventListener('click', e =>{
        console.log("Remove data");
        e.preventDefault;
        deleteModalElement.style.display="block";
      });
  
      // Add event listener when delete form is submited
      deleteDataFormElement.addEventListener('submit', (e) => {
        // delete data (readings)
        dbRef.remove();
      });
  
      // TABLE
      var lastReadingTimestamp; //saves last timestamp displayed on the table
      // Function that creates the table with the first 100 readings
      function createTable(){
        // append all data to the table
        var firstRun = true;
        dbRef.orderByKey().limitToLast(100).on('child_added', function(snapshot) {
          if (snapshot.exists()) {
            var jsonData = snapshot.toJSON();
            console.log(jsonData);
            var temperature = jsonData.temperature;
            var humidity = jsonData.humidity;
            var pressure = jsonData.pressure;
            var timestamp = jsonData.timestamp;
            var content = '';
            content += '<tr>';
            content += '<td>' + epochToDateTime(timestamp) + '</td>';
            content += '<td>' + temperature + '</td>';
            content += '<td>' + humidity + '</td>';
            content += '<td>' + pressure + '</td>';
            content += '</tr>';
            $('#tbody').prepend(content);
            // Save lastReadingTimestamp --> corresponds to the first timestamp on the returned snapshot data
            if (firstRun){
              lastReadingTimestamp = timestamp;
              firstRun=false;
              console.log(lastReadingTimestamp);
            }
          }
        });
      };
  
      // append readings to table (after pressing More results... button)
      function appendToTable(){
        var dataList = []; // saves list of readings returned by the snapshot (oldest-->newest)
        var reversedList = []; // the same as previous, but reversed (newest--> oldest)
        console.log("APEND");
        dbRef.orderByKey().limitToLast(100).endAt(lastReadingTimestamp).once('value', function(snapshot) {
          // convert the snapshot to JSON
          if (snapshot.exists()) {
            snapshot.forEach(element => {
              var jsonData = element.toJSON();
              dataList.push(jsonData); // create a list with all data
            });
            lastReadingTimestamp = dataList[0].timestamp; //oldest timestamp corresponds to the first on the list (oldest --> newest)
            reversedList = dataList.reverse(); // reverse the order of the list (newest data --> oldest data)
  
            var firstTime = true;
            // loop through all elements of the list and append to table (newest elements first)
            reversedList.forEach(element =>{
              if (firstTime){ // ignore first reading (it's already on the table from the previous query)
                firstTime = false;
              }
              else{
                var temperature = element.temperature;
                var humidity = element.humidity;
                var pressure = element.pressure;
                var timestamp = element.timestamp;
                var content = '';
                content += '<tr>';
                content += '<td>' + epochToDateTime(timestamp) + '</td>';
                content += '<td>' + temperature + '</td>';
                content += '<td>' + humidity + '</td>';
                content += '<td>' + pressure + '</td>';
                content += '</tr>';
                $('#tbody').append(content);
              }
            });
          }
        });
      }
  
      viewDataButtonElement.addEventListener('click', (e) =>{
        // Toggle DOM elements
        tableContainerElement.style.display = 'block';
        viewDataButtonElement.style.display ='none';
        hideDataButtonElement.style.display ='inline-block';
        loadDataButtonElement.style.display = 'inline-block'
        createTable();
      });
  
      loadDataButtonElement.addEventListener('click', (e) => {
        appendToTable();
      });
  
      hideDataButtonElement.addEventListener('click', (e) => {
        tableContainerElement.style.display = 'none';
        viewDataButtonElement.style.display = 'inline-block';
        hideDataButtonElement.style.display = 'none';
      });
  
    // IF USER IS LOGGED OUT
    } else{
      // toggle UI elements
      loginElement.style.display = 'block';
      authBarElement.style.display ='none';
      userDetailsElement.style.display ='none';
      contentElement.style.display = 'none';
    }
  }
  
