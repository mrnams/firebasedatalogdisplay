// Create Temperature Gauge
function createTemperatureGauge() {
    var gauge = new LinearGauge({
        renderTo: 'gauge-temperature',
        width: 120,
        height: 400,
        units: "Temperature °C",
        minValue: -20,
        startAngle: 90,
        ticksAngle: 180,
        maxValue: 40,
        colorValueBoxRect: "#049faa",
        colorValueBoxRectEnd: "#049faa",
        colorValueBoxBackground: "#f1fbfc",
        valueDec: 2,
        valueInt: 2,
        majorTicks  : ['-20','-10','0','10','20','30','40'],
        minorTicks: 4,
        strokeTicks: true,
        highlights: [
            {
                "from": 30,
                "to": 40,
                "color": "rgba(200, 50, 50, .3)"
            },{
                "from":0,
                "to":10,
                "color":"rgba(0,50,250,0.75)"
            },{
                "from":-20,
                "to":0,
                "color":"rgba(79,192,247,0.5)"
            },{
                "from":10,
                "to":30,
                "color":"rgba(0,255,0,0.3)"
            }
        ],
        colorPlate: "#fff",
        colorBarProgress: "#CC2936",
        colorBarProgressEnd: "#049faa",
        borderShadowWidth: 0,
        borders: false,
        needleType: "arrow",
        needleWidth: 2,
        needleCircleSize: 7,
        needleCircleOuter: true,
        needleCircleInner: false,
        animationDuration: 1500,
        animationRule: "linear",
        barWidth: 10,
    });
    return gauge;
}

// Create Humidity Gauge
function createHumidityGauge(){
    var gauge = new RadialGauge({
        renderTo: 'gauge-humidity',
        width: 200,
        height: 200,
        units: "Relative Humidity (%)",
        minValue: 0,
        maxValue: 100,
        colorValueBoxRect: "#049faa",
        colorValueBoxRectEnd: "#049faa",
        colorValueBoxBackground: "#f1fbfc",
        valueInt: 2,
        majorTicks: [
            "0",
            "20",
            "40",
            "60",
            "80",
            "100"
    
        ],
        minorTicks: 4,
        strokeTicks: true,
        highlights: [
            {
                "from": 0,
                "to":30,
                "color":"#cccccc"
            },
            {
                "from": 30,
                "to": 60,
                "color": "#03C0C1"
            },{
                "from":70,
                "to":100,
                "color":"#ff0000"
            },{
                "from":60,
                "to":70,
                "color":"#ffC000"
            }
        ],
        colorPlate: "#fff",
        borderShadowWidth: 0,
        borders: false,
        needleType: "line",
        colorNeedle: "#007F80",
        colorNeedleEnd: "#007F80",
        needleWidth: 2,
        needleCircleSize: 3,
        colorNeedleCircleOuter: "#007F80",
        needleCircleOuter: true,
        needleCircleInner: false,
        animationDuration: 1500,
        animationRule: "linear"
    });
    return gauge;
}

// Create Pressure Gauge
function createPressureGauge(){
    var gauge = new RadialGauge({
        renderTo: 'gauge-pressure',
        width: 200,
        height: 200,
        units: "Pressure (hPa)",
        minValue: 940,
        maxValue: 1100,
        colorValueBoxRect: "#049faa",
        colorValueBoxRectEnd: "#049faa",
        colorValueBoxBackground: "#f1fbfc",
        valueInt: 2,
        majorTicks: [
            "940",
            "960",
            "980",
            "1000",
            "1020",
            "1040",
            "1060",
            "1080",
            "1100"
    
        ],
        minorTicks: 4,
        strokeTicks: true,
        highlights: [
            {
                "from":940,
                "to":953,
                "color":"#39e75f"
            },
            {
                "from": 953,
                "to": 1060,
                "color": "#03C0C1"
            },{
                "from":1060,
                "to": 1100,
                "color":"#FDC0C1"
            }
        ],
        colorPlate: "#fff",
        borderShadowWidth: 0,
        borders: false,
        needleType: "line",
        colorNeedle: "#007F80",
        colorNeedleEnd: "#007F80",
        needleWidth: 2,
        needleCircleSize: 3,
        colorNeedleCircleOuter: "#007F80",
        needleCircleOuter: true,
        needleCircleInner: true,
        animationDuration: 1500,
        animationRule: "linear"
    });
    return gauge;
}


// Create mmHgPressure Gauge in mmHG
function createmmHgGauge(){
    var gauge = new RadialGauge({
        renderTo: 'gauge-mmHg',
        width: 200,
        height: 200,
        units: "Pressure (mmHg)",
        minValue: 700,
        maxValue: 840,
        colorValueBoxRect: "#049faa",
        colorValueBoxRectEnd: "#049faa",
        colorValueBoxBackground: "#f1fbfc",
        valueInt: 2,
        majorTicks: [
            "700",
            "720",
            "740",
            "760",
            "780",
            "800",
            "820",
            "840"
    
        ],
        minorTicks: 4,
        strokeTicks: true,
        highlights: [
            {"from":700,
            "to":714,
                "color":"#5ced73"
            },
            {
                "from": 714,
                "to": 795,
                "color": "#03C0C1"
            },{
                "from":795,
                "to": 840,
                "color":"#FDC0C1"
            }
        ],
        colorPlate: "#fff",
        borderShadowWidth: 0,
        borders: false,
        needleType: "line",
        colorNeedle: "#007F80",
        colorNeedleEnd: "#007F80",
        needleWidth: 2,
        needleCircleSize: 3,
        colorNeedleCircleOuter: "#007F80",
        needleCircleOuter: true,
        needleCircleInner: true,
        animationDuration: 1500,
        animationRule: "linear"
    });
    return gauge;
}

// Create DewTemperature Gauge
function createDewGauge(){
    var gauge = new RadialGauge({
        renderTo: 'gauge-dew',
        width: 200,
        height: 200,
        units: "Dew temperature C",
        minValue: 0,
        maxValue: 40,
        colorValueBoxRect: "#049faa",
        colorValueBoxRectEnd: "#049faa",
        colorValueBoxBackground: "#f1fbfc",
        valueInt: 2,
        majorTicks: [
            "0",
            "10",
            "20",
            "30",
            "40"
    
        ],
        minorTicks: 4,
        strokeTicks: true,
        highlights: [
            {"from":10,
            "to":15,
                "color":"#5ced73"
            },
            {
                "from": 15,
                "to": 20,
                "color": "#03C0C1"
            },{
                "from":20,
                "to": 40,
                "color":"#FDC0C1"
            }
        ],
        colorPlate: "#fff",
        borderShadowWidth: 0,
        borders: false,
        needleType: "line",
        colorNeedle: "#007F80",
        colorNeedleEnd: "#007F80",
        needleWidth: 2,
        needleCircleSize: 3,
        colorNeedleCircleOuter: "#007F80",
        needleCircleOuter: true,
        needleCircleInner: true,
        animationDuration: 1500,
        animationRule: "linear"
    });
    return gauge;
}